package ictgradschool.web.lab15.ex1;

import org.omg.CORBA.Request;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;

public class LoggingTableNewEntry extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String name = request.getParameter("name");
        String desc = request.getParameter("desc");

        try{
            AccessLogDAO a = new AccessLogDAO();
            a.postEntries(name, desc);

            response.sendRedirect("/question1");


        } catch (Exception e){
            System.out.println(e.getMessage());
        }




        //response.sendRedirect(getServletContext().getContextPath()+"/question1");
       //doGet(request, response);


//        response.sendRedirect("../LoggingTable");
//        System.out.println("before");
//
//        RequestDispatcher rd = getServletContext().getRequestDispatcher("LoggingTable");
//        System.out.println("Dispatcher initialised");
//
//
//
//        rd.forward(request, response);
//        System.out.println("Done");
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
        PrintWriter pw = response.getWriter();

        pw.print("<h1>405 - GET method not supported</h1>");

        pw.close();
    }
}
