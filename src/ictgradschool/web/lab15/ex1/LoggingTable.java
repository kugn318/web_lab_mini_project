package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class LoggingTable extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter pw = response.getWriter();

               pw.print("<style>\n"+
        "table,tr,td{\n" +
        "border-collapse: collapse;\n"+
        "border: 1px solid black;\n"+
        "th,td {\n"+
        "padding: 10px;\n"+
        "\n"+"</style>");

        pw.print("<p>Display your table and form here</p>");
        pw.print("<form action =\"/question1/new\" method=\"post\">");
        pw.print("<label for =\"name\"> Name:</label>");
        pw.print("<input type=\"text\" id=\"name\" name=\"name\">");
        pw.print("<label for =\"desc\"> Description:</label>");
        pw.print("<input type=\"text\" id=\"desc\" name=\"desc\">");
        pw.print("<input type=\"submit\" value=\"Submit\">\n");
       // pw.print("<button action=\"/question1\" method=\"get\">Update</button>");


        try {
            AccessLogDAO a = new AccessLogDAO();
            List<AccessLogEntry> entries = a.getLogEntries();

            response.setContentType("text/html");
            pw.print("<table>");
            for (AccessLogEntry e: entries ){
                pw.print("<tr>");
                pw.print("<td>"+e.getId()+"</td>");
                pw.print("<td>"+e.getName()+"</td>");
                pw.print("<td>"+e.getDes()+"</td>");
                pw.print("<td>"+e.getTime()+"</td>");
                pw.print("</tr>");
            }
            pw.print("</table>");
        }
        catch (Exception e){
            System.out.println(e.getMessage());
        }

        pw.close();
    }
}
