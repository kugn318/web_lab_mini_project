package ictgradschool.web.lab15.ex1;

import com.zaxxer.hikari.pool.HikariProxyConnection;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class AccessLogDAO {

//    //create a property
//    Properties dbProps = new Properties();
//
//    //connect with mysql.properties file
//        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
//        dbProps.load(fIn);
//    } catch (IOException e) {
//        e.printStackTrace();
//    }

    // the actual connection
//        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
//
//    }catch (SQLException e) {
//        e.printStackTrace();
//    }

    private final Connection conn;

    public AccessLogDAO() throws SQLException{
        this.conn = HikariConnectionPool.getConnection();
    }

    public void postEntries(String name, String des) throws SQLException {

        try (PreparedStatement smt = this.conn.prepareStatement("INSERT INTO access_log (name, description) VALUES (?, ? )")) {

            smt.setString(1, name);
            smt.setString(2, des);

            smt.executeUpdate();

            }
        }


    public List<AccessLogEntry> getLogEntries() throws SQLException{
        List<AccessLogEntry> entries = new ArrayList<>();

        try (PreparedStatement smt = this.conn.prepareStatement("SELECT* FROM access_log;")){

            try(ResultSet rs = smt.executeQuery()){

                while (rs.next()){
                    AccessLogEntry newLog = new AccessLogEntry();

                    newLog.setId(rs.getInt(1));
                    newLog.setName(rs.getString(2));
                    newLog.setDes(rs.getString(3));
                    newLog.setTime_stampt(rs.getDate(4));

                    entries.add(newLog);
                }
            }

        }

        return entries;
    }

}
